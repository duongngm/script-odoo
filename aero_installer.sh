#!/bin/bash
set -e

AEROO_LIBS="git \
            openjdk-8-jdk openjdk-8-jre-headless \
            python-genshi \
            python-cairo \
            python-lxml \
            libreoffice-script-provider-python \
            libreoffice-base \
            libreoffice-writer \
            libreoffice-calc \
            python-cups \
            python-setuptools \
            python3-uno \
            python3-pip \
            xvfb \
            "

DPKG_DEPENDS=${AEROO_LIBS}
PIP_DPKG_BUILD_DEPENDS=""
DPKG_UNNECESSARY=""

PIP_OPTS="--upgrade \
          --no-cache-dir"

echo -e "\n---- Install Packages For Aeroo Report ----"
apt-get update
# apt-get upgrade -y
apt-get install -y --no-install-recommends ${DPKG_DEPENDS} ${PIP_DPKG_BUILD_DEPENDS}

pip3 install ${PIP_OPTS} git+http://gitlab.besco.vn/thanh/aeroolib.git@py3

#Create Init Script for OpenOffice (Headless Mode):
echo -e "\n---- create init script for LibreOffice (Headless Mode) ----"
cat <<EOT >> /etc/init.d/office
#! /bin/sh
### BEGIN INIT INFO
# Provides:          office
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO
/usr/bin/soffice --nologo --nofirststartwizard --headless --norestore --invisible "--accept=socket,host=localhost,port=8100,tcpNoDelay=1;urp;StarOffice.ComponentContext" &
EOT

# Setup Permissions and test LibreOffice Headless mode connection
chmod +x /etc/init.d/office
update-rc.d office defaults

# Install AerooDOCS
echo -e "\n---- Install AerooDOCS (see: https://github.com/aeroo/aeroo_docs/wiki/Installation-example-for-Ubuntu-14.04-LTS for original post): ----"
pip3 install jsonrpc2 daemonize request

echo -e "\n---- create conf file for AerooDOCS ----"
cat <<EOT >> /etc/aeroo-docs.conf
[start]
interface = localhost
port = 8989
oo-server = localhost
oo-port = 8100
spool-directory = /tmp/aeroo-docs
spool-expire = 1800
log-file = /var/log/aeroo-docs/aeroo_docs.log
pid-file = /tmp/aeroo-docs.pid
[simple-auth]
username = anonymous
password = anonymous
EOT

git clone https://github.com/aeroo/aeroo_docs.git /opt/aeroo/aeroo_docs
# python3 /opt/aeroo/aeroo_docs/aeroo-docs start -c /etc/aeroo-docs.conf # If you encounter and error "Unable to lock on the pidfile while trying #16 just restart the service (service aeroo-docs restart).
ln -s /opt/aeroo/aeroo_docs/aeroo-docs /etc/init.d/aeroo-docs
update-rc.d aeroo-docs defaults
service aeroo-docs restart

echo -e "\n---- Install requirements of Aeroo Reports Odoo Modules: ----"
curl -s https://raw.githubusercontent.com/aeroo/aeroo_reports/11.0/requirements.txt | pip3 install -r /dev/stdin

# Remove build depends for pip
echo -e "\n---- Remove build depends for pip ----"
apt-get purge ${PIP_DPKG_BUILD_DEPENDS} ${DPKG_UNNECESSARY}

